###
# fuse-mediawiki - FUSE filesystem for editing MediaWiki websites
# Copyright (C) 2008  Ian Weller <ianweller@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###

"""fuse-mediawiki provides a FUSE filesystem for MediaWiki websites."""

import fuse
from fuse import Fuse
import os
import sys
import stat
import errno
import time
import urllib
import urllib2
import cookielib
import simplejson
import re
from StringIO import StringIO
from getpass import getpass
import base64


class FuseMediaWiki(Fuse):
    """Class to provide FUSE filesystem."""

    # The filesystem used is simply a dict of keys (filenames) and values.
    # For attrs, the value of a key is a specific instance of fuse.Stat().
    attrs = {}
    # For files, the value of a key is the contents of that file.
    files = {}
    # Here are some edit tokens that might help (wikititle: token)
    edittoken = {}
    timestamp = {}

    def __init__(self, version, usage):
        # init fuse
        fuse.fuse_python_api = (0, 2)
        Fuse.__init__(self, version=version, usage=usage)
        # init default file attributes
        for path in ['/', '/image', '/cat', '/content']:
            self._setdirattr(path)
        # init file system and default directories
        self.files['/image'] = 0
        self.files['/cat'] = 0
        self.files['/content'] = 0
        # init cookie jar
        self.cookiejar = cookielib.LWPCookieJar()
        urllib2.install_opener(urllib2.build_opener(
            urllib2.HTTPCookieProcessor(self.cookiejar)))

    def _file2fd(self, path):
        """Create a file descriptor out of a file within this filesystem"""
        try:
            return StringIO(self.files[path])
        except KeyError:
            return StringIO()

    def _unescape(self, text):
        """Unescape text in the textarea on retrieval"""
        text = text.replace('&lt;', '<')
        text = text.replace('&gt;', '>')
        text = text.replace('&amp;', '&')
        return text

    def _setdirattr(self, path):
        """Set default attributes for a directory at the specified path"""
        self.attrs[path] = fuse.Stat()
        self.attrs[path].st_mode = stat.S_IFDIR | 0755 # drwxr-xr-x
        self.attrs[path].st_uid = int(os.getuid())
        self.attrs[path].st_gid = int(os.getgid())
        self.attrs[path].st_size = 4096 # 4.0 K
        self.attrs[path].st_atime = time.time()
        self.attrs[path].st_mtime = time.time()
        self.attrs[path].st_ctime = time.time()
        self.attrs[path].st_nlink = 2

    def _setregattr(self, path):
        """Set default attributes for a regular file at the specified path"""
        self.attrs[path] = fuse.Stat()
        self.attrs[path].st_mode = stat.S_IFREG | 0644 # -rw-r--r--
        self.attrs[path].st_uid = int(os.getuid())
        self.attrs[path].st_gid = int(os.getgid())
        self.attrs[path].st_size = 0
        self.attrs[path].st_atime = time.time()
        self.attrs[path].st_mtime = time.time()
        self.attrs[path].st_ctime = time.time()
        self.attrs[path].st_nlink = 1

    def _log(self, message):
        """Log a message. Currently it just uses print, and therefore logs only
        if the -f or -d options are sent from the command line."""
        # it's simple now, but in case we want to change it later... here we go
        print message
        return

    def _setrooturl(self, url):
        self.rooturl = url
        return

    def _apifetch(self, apivars=None, headers={}, format="json"):
        apivars["format"] = format
        post = urllib.urlencode(apivars)
        request = urllib2.Request(self.rooturl, post)
        for header in headers:
            request.add_header(header, headers[header])
        response = urllib2.urlopen(request)
        return (response, request)

    def _setupauth(self, username):
        # if there is no username, anonymous auth
        if username == None:
            self._log("Anonymously accessing wiki")
        else:
            self.username = username
            self._asklogin()
        return

    def _asklogin(self):
        self.password = getpass(self.username + "'s password: ")
        return self._login()

    def _login(self):
        """Attempts to login to the wiki."""
        print "Logging in..."
        apivars = {'lgname': self.username, 'lgpassword': self.password,
                   'action': 'login'}
        response = self._apifetch(apivars)[0]
        data = simplejson.loads(response.read())
        if data['login']['result'] == "Success":
            # login OK
            self._log("Logged in successfully as %s" % self.username)
            return True
        elif data['login']['result'] == "Throttled":
            # oh god we're throttling!
            self._log("The API thinks we're throttling, please wait %i seconds"
                      % data['login']['wait'])
            sys.exit()
        else:
            # bad login
            print "Login failed"
            sys.exit()

    def getattr(self, path):
        if path != '/':
            self._log('*** getattr '+path)
        if path in self.attrs:
            # file found
            return self.attrs[path]
        elif path[-5:] == ".wiki" and path[:8] == "/content":
            # file technically found... we need to go make it.
            self.mknod(path, 0100644, 0)
            return self.attrs[path]
        else:
            # file not found
            return -errno.ENOENT

    def getdir(self, path):
        self._log('*** getdir '+path)
        keys = self.files.keys()
        flist = []
        for key in keys:
            l = len(path)
            print (key, key[0:l], key[l:], key[l+1:])
            if key[0:l] == path:
                if key[l:] != '':
                    if '/' == key[l]:
                        if '/' not in key[l+1:]:
                            flist.append((key[l+1:], 0))
                    else:
                        if '/' not in key[l:]:
                            flist.append((key[l:], 0))
        flist.append(('.', 0))
        flist.append(('..', 0))
        return flist

    def readdir(self, path, offset):
        self._log('*** readdir '+str([path, offset]))
        return self.readdir_compat_0_1(path, offset)

    def mythread(self):
        self._log('*** mythread')
        return -errno.ENOSYS

    def chmod(self, path, mode):
        self._log('*** chmod '+str([path, oct(mode)]))
        self.attrs[path].st_mode = mode
        return

    def chown(self, path, uid, gid):
        self._log('*** chown '+str([path, uid, gid]))
        return -errno.ENOSYS

    def fsync(self, path, isFsyncFile, fd=None):
        self._log('*** fsync '+str([path, isFsyncFile, fd]))
        return

    def link(self, targetPath, linkPath):
        self._log('*** link '+str([targetPath, linkPath]))
        return -errno.ENOSYS

    def mkdir(self, path, mode):
        """Create a directory."""
        self._log('*** mkdir '+str([path, oct(mode)]))
        if re.match('^/image/.*$', path):
            return -errno.EPERM
        if re.match('^/cat/.*$', path):
            return -errno.ENOSYS
        self.files[path] = 0
        self._setdirattr(path)
        self.attrs[path].st_mode = stat.S_IFDIR | mode
        return

    def mknod(self, path, mode, dev):
        """Create a file. Not sure what the dev argument is, but it doesn't
        seem useful."""
        self._log('*** mknod '+str([path, oct(mode), dev]))
        self.files[path] = ""
        self._setregattr(path)
        self.attrs[path].st_mode = mode
        if path[-5:] == '.wiki' and path[:8] == "/content":
            # this is a wiki page, get page contents
            wikititle = path[9:-5]
            apivars = {'action': 'query', 'titles': wikititle, 'prop':
                       'info|revisions', 'rvprop': 'content|timestamp',
                       'intoken': 'edit'}
            (response, request) = self._apifetch(apivars)
            data = simplejson.loads(response.read())
            page = data['query']['pages'][data['query']['pages'].keys()[0]]\
                    ['revisions'][0]['*'].encode('utf8')
            print type(page)
            self.files[path] = page
            self.attrs[path].st_size = len(page)
            self.edittoken[path] = data['query']['pages'][data['query']\
                    ['pages'].keys()[0]]['edittoken']
            self.timestamp[path] = data['query']['pages'][data['query']\
                    ['pages'].keys()[0]]['revisions'][0]['timestamp']
        return

    def open(self, path, flags):
        self._log('*** open '+str([path, flags]))
        return self._file2fd(path)

    def read(self, path, length, offset, fd=None):
        self._log('*** read'+str([path, length, offset, fd]))
        if fd != None:
            # we'll just read from the provided StringIO
            fd.seek(offset)
            return fd.read(length)
        else:
            # this should never really happen
            try:
                cwd = self.files[path]
            except KeyError:
                return -errno.ENOENT
            return cwd[offset:length]

    def readlink(self, path):
        self._log('*** readlink '+path)
        return -errno.ENOSYS

    def release(self, path, flags, fd=None):
        self._log('*** release '+str([path, flags, fd]))
        if fd != None:
            fd.close()
            return
        else:
            # i don't think we really care
            return

    def rename(self, oldPath, newPath):
        self._log('*** rename '+str([oldPath, newPath]))
        self.files[newPath] = self.files[oldPath]
        del self.files[oldPath]
        self.attrs[newPath] = self.attrs[oldPath]
        del self.attrs[oldPath]
        return

    def rmdir(self, path):
        self._log('*** rmdir '+path)
        # -errno.ENOTEMPTY will be useful
        return -errno.ENOSYS

    def statfs(self):
        #self._log('*** statfs')
        vfs = fuse.StatVfs()
        return vfs

    def symlink(self, targetPath, linkPath):
        self._log('*** symlink '+str([targetPath, linkPath]))
        return -errno.ENOSYS

    def truncate(self, path, size):
        self._log('*** truncate '+str([path, size]))
        self.files[path] = self.files[path][0:size]
        self.attrs[path].st_size = len(self.files[path])
        return None

    def unlink(self, path):
        self._log('*** unlink '+path)
        if path not in self.files:
            return -errno.ENOENT
        del self.attrs[path]
        del self.files[path]
        return

    def utime(self, path, times):
        self._log('*** utime '+str([path, times]))
        self.attrs[path].st_atime = times[0]
        self.attrs[path].st_mtime = times[1]
        return

    def write(self, path, buf, offset, fd=None):
        self._log('*** write'+str([path, len(buf), offset, fd]))
        if fd != None:
            fd.seek(offset)
            fd.write(buf)
        x = self.files[path]
        self.files[path] = x[:offset] + buf + x[offset:]
        self.attrs[path].st_size = len(self.files[path])
        if path[-5:] == '.wiki' and path[:8] == "/content":
            # this is a wiki page, save page contents
            editsumm = self._re_editsumm.search(self.files[path]).group(1)
            editsumm = editsumm.strip()
            data = self._re_fmwcomm.sub('', self.files[path]).strip()
            wikititle = path[9:-5]
            getvars = {'title': wikititle, 'action': 'submit'}
            postvars = {'wpSection': '', 'wpStarttime':
                        self.wikitokens[wikititle]['start'], 'wpEdittime':
                        self.wikitokens[wikititle]['edit'], 'wpScrolltop': '0',
                        'wpTextbox1': data, 'wpSummary': editsumm,
                        'wpSave': 'Save page', 'wpEditToken':
                        self.wikitokens[wikititle]['token'], 'wpAutoSummary':
                        self.wikitokens[wikititle]['auto']}
            response = self._urlfetch(getvars, postvars)[0]
            respdata = response.read()
        return len(buf)


def main():
    usage = """Usage: python %prog [OPTIONS] ROOT_URL MOUNTPOINT
fuse-mediawiki is a FUSE filesystem for editing MediaWiki websites.
"""
    # setup FUSE
    fs = FuseMediaWiki(version="fuse-mediawiki", usage=usage)
    # setup option parser
    # -u, --username: set the username. password is asked for on mount
    fs.parser.add_option("-u", "--username", dest="username",
                         help="username for login into wiki")
    # allow for FUSE mount options
    fs.parser.mountopt = True
    # get arguments
    (options, args) = fs.parser.parse_args()
    # if we have zero or two or more arguments before the MOUNTPOINT, fail.
    if len(args) != 1:
        print "Must specify exactly 1 api.php URL and 1 mount point, " \
                "in that order"
        print "api.php URL example: http://en.wikipedia.org/w/api.php"
        sys.exit(1)
    # set the root URL
    fs._setrooturl(args[0])
    # tell the object what are authentication method is
    fs._setupauth(options.username)
    # this does something, I'm not really sure what at the moment
    fs.parse(values=fs, errex=1)
    # go into main loop
    fs.main()


# hi!
if __name__ == '__main__':
    main()

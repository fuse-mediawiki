fuse-mediawiki is an attempt to create a FUSE-compatible filesystem that allows
you to read, edit, move, and otherwise play around with wikis that run on the
MediaWiki platform.  An attempt will be made to offer every feature that is
possibly available in a filesystem that also works on the wiki *except*
deletion, which I've deemed too dangerous for a simple filesystem.

Usage:
  python fuse-mediawiki.py [-t TIMEOUT] [-u USERNAME] [-l LOG_FILE] \
    [--http-basic] [-o FUSEOPTS] ROOT_URL MOUNTPOINT

  The root URL is usually something that ends in something like /w/index.php.
  If you have trouble finding it, try editing or viewing the history of a
  page... it should end in index.php without any options.

Example:
 python fuse-mediawiki.py [-u USERNAME] \
   http://en.wikipedia.org/w/index.php ~/wiki/
This will mount the English Wikipedia into $HOME/wiki/.

If your wiki uses HTTP Basic authentication, pass the --http-basic option.

To edit an article, open the name of the article with a .wiki extension in your
favorite text editor; i.e.,
 vim ~/wiki/Main_Page.wiki
If your article is nested under another article (say, [[Main Page/new]]):
 mkdir ~/wiki/Main_Page
 vim ~/wiki/Main_Page/new.wiki

If you encounter a problem, try and reproduce it with the -f flag at the end of
your command line, and copy and paste the problematic-looking part. See HACKING
for more information.

==============================================================================

Yes, I know that the wikipediafs project exists... however, it didn't do
everything I wish it would, and it wouldn't work with the wiki I was attempting
to use it with (http://fedoraproject.org/wiki/), because of how authorization
was done on that wiki.  So, here comes this project.

Anybody who can help is greatly appreciated.  Send me an email, a patch, or
whatever.

May 28, 2008
Ian Weller <ianweller@gmail.com>

==============================================================================

KNOWN BUGS (other than the filesystem itself not working as it should)

- On --help, usage and non-FUSE options are echoed twice.
